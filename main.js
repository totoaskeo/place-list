// Form
Vue.component('app-form', {
  template: '#app-form'
});

// List
Vue.component('app-list', {
  template: '#app-list',
  props: ['list']
});

// Map
Vue.component('app-map', {
  template: '#app-map'
});

// Local Storage
var placeStorage = {
  fetch: function() {
    var list = JSON.parse(localStorage.getItem('places')) || [];
    return list;
  },
  store: function(list) {
    localStorage.setItem('places', JSON.stringify(list));
  }
}

// Map
var myMap;
var placemarks = [];
ymaps.ready(init);
function init() {
  myMap = new ymaps.Map('map', {
    center: [59.93, 30.33],
    zoom: 10
  });
  if (app.list.length === 0) return; // No initial places
  for (let i = 0; i < app.list.length; i++) {
    var placemark = new ymaps.GeoObject({
      geometry: {
        type: "Point",
        coordinates: [app.list[i].geocode[0], app.list[i].geocode[1]]
      },
      properties: {
        hintContent: app.list[i].address,
        iconCaption: app.list[i].title
      }
    }, {
      preset: 'islands#blueCircleDotIconWithCaption'
    });
    placemarks.push(placemark);
    myMap.geoObjects.add(placemark);
  }
}

function drawOnMap(res, title, address) {
  var object = res.geoObjects.get(0),
      coords = object.geometry.getCoordinates(),
      bounds = object.properties.get('boundedBy');
  object.options.set('preset', 'islands#blueDotIconWithCaption');
  object.properties.set('iconCaption', title);
  object.properties.set('hintCaption', address);
  myMap.geoObjects.add(object);
  myMap.setBounds(bounds, {
    checkZoomRange: true
  });
  placemarks.push(object);
  return object;
}

// Vue
var app = new Vue({
  el: '#app',
  data: {
    // Model
    title: '',
    address: '',
    submitted: false,
    hasErrors: false,
    // Props
    list: placeStorage.fetch(),
  },
  watch: {
    list: {
      handler: function(list) {
        placeStorage.store(list);
      }
    }
  },
  computed: {
    isTitleMissing: function() { return this.title === ''; },
    isAddressMissing: function() { return this.address === ''; }
  },
  methods: {
    validate: function() {
      this.submitted = true;
      if (this.isTitleMissing || this.isAddressMissing) { this.hasErrors = true; return; }
      else {
        this.savePlace();
      }
    },
    savePlace: function() {
      ymaps.geocode(this.address, {
        results: 1
      }).then((res) => {
        var object = drawOnMap(res, this.title, this.address);
        // Save to list and clear input fields
        this.list.push({
          title: this.title,
          address: this.address,
          visited: false,
          geocode: object.geometry.getCoordinates()
        });
        this.title = '';
        this.address = '';
        this.submitted = false;
      });
    },
    deletePlace: function(index) {
      this.list.splice(index, 1);
      myMap.geoObjects.remove(placemarks[index]);
      placemarks.splice(index, 1);
    },
    toggleVisited(index, isVisited) {
      if (isVisited) {
        placemarks[index].options.set('preset', 'islands#greenCircleDotIconWithCaption');
      } else {
        placemarks[index].options.set('preset', 'islands#blueCircleDotIconWithCaption');
      }
    },
    showOnMap: function(index) {
      var bounds = placemarks[index].geometry.getBounds();
      console.log(bounds);
      myMap.setBounds(bounds, {
        checkZoomRange: true
      });
    }
  }
});
